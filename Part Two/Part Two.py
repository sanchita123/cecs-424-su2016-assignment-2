import sys , math 


#checks whether the input number is prime or composite

def is_prime(num):
   
   if num < 2 :
    return False

   if num == 2 :
	 return True

   else :
    for div in range(2,int(math.sqrt(num))+1):
		if num % div ==0:

				return False
				
				
   return True
  


# This section of code displays all prime numbers upto the entered input.(Tried to execute as upper range = 2 millions but it was taking indefinite time) 

def get_primes(num): 
	if num==2: return [2]
	elif num<2: return []
	s=range(3,num+1,2)
	base = num ** 0.5
	half=(num+1)/2-1
	i=0
	k=3
	while k <= base:
		if s[i]:
			j=(k*k-3)/2
			s[j]=0
			while j<half:
				s[j]=0
				j+=k
		i=i+1
		k=2*i+3
	
	return [2]+[x for x in s if x]



# This section of code gives the sum of prime numbers as per Euler's Problem (sum of all the primes below two million.)


upper_bound = 2000000
	
def sum_primes(upper_bound):
    primes = []
    for n in xrange(2, upper_bound+1):
        # try dividing n with all primes less than sqrt(n):
        for prm in primes:
            if n % prm == 0: break     # if prm divides n, stop the search
            if n < prm*prm:
               primes.append(n)      # if prm > sqrt(n), mark n as prime and stop search
               break
        else: primes.append(n)       
    return sum(primes)
  



#----------Execution part of the code-----


inputval = input('Enter the input value to check ')

print('You have entered - :' +str(inputval))

n = int(inputval)
#print('value of n is :-'+str(n))
num = int(n)

print(is_prime(num))


if is_prime(num):
 	print get_primes(num)


print ('The sum of all primes below two million is ') 
print sum_primes(2000000)


	
